package com.santiago.java.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.santiago.java.controller.product.PriceQueryController;
import com.santiago.java.models.product.PriceQuery;
import com.santiago.java.models.product.Product;
import com.santiago.java.repository.product.ProductRepository;
import com.santiago.java.service.product.PriceQueryService;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class TestApplication {

	@Mock
	private PriceQueryService priceQueryService;

	@InjectMocks
	private PriceQueryController priceQueryController;

	@Autowired
	private ProductRepository productRepository;

	@Test
	public void testDatabaseHasData() {
		Stream<Product> productStream = StreamSupport.stream(productRepository.findAll().spliterator(), false);
		boolean hasData = productStream.anyMatch(product -> true);
		assertFalse(hasData, "La base de datos no tiene datos");
	}

	@Test
	public void testPriceAt10AMOnDay14() throws ParseException {
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-06-14 10:00:00");
		Long productId = 35455L;
		Long brandId = 1L;
		List<PriceQuery> expectedResult = new ArrayList<>();
		expectedResult.add(createPriceQuery(35455L, 1L, 1L, date, parseDate("2020-12-31 23:59:59"), 35.50));

		when(this.priceQueryService.queryPrice(eq(date), eq(productId), eq(brandId))).thenReturn(expectedResult);

		List<PriceQuery> result = this.priceQueryController.queryPrice(date, productId, brandId);

		if (result.size() >= 1) {
			assertEquals(expectedResult.size(), result.size());
			assertEquals(expectedResult.get(0).getProductId(), result.get(0).getProductId());
			assertEquals(expectedResult.get(0).getBrandId(), result.get(0).getBrandId());
			assertEquals(expectedResult.get(0).getPriceList(), result.get(0).getPriceList());
			assertEquals(expectedResult.get(0).getStartDate(), result.get(0).getStartDate());
			assertEquals(expectedResult.get(0).getEndDate(), result.get(0).getEndDate());
			assertEquals(expectedResult.get(0).getPrice(), result.get(0).getPrice());
		}
	}

	@Test
	public void testPriceAt4PMOnDay14() throws ParseException {
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-06-14 16:00:00");
		Long productId = 35455L;
		Long brandId = 1L;
		List<PriceQuery> expectedResult = new ArrayList<>();
		expectedResult.add(createPriceQuery(35455L, 1L, 2L, date, parseDate("2020-06-14 18:30:00"), 25.45));

		when(this.priceQueryService.queryPrice(eq(date), eq(productId), eq(brandId))).thenReturn(expectedResult);

		List<PriceQuery> result = this.priceQueryController.queryPrice(date, productId, brandId);

		if (result.size() >= 1) {
			assertEquals(expectedResult.size(), result.size());
			assertEquals(expectedResult.get(0).getProductId(), result.get(0).getProductId());
			assertEquals(expectedResult.get(0).getBrandId(), result.get(0).getBrandId());
			assertEquals(expectedResult.get(0).getPriceList(), result.get(0).getPriceList());
			assertEquals(expectedResult.get(0).getStartDate(), result.get(0).getStartDate());
			assertEquals(expectedResult.get(0).getEndDate(), result.get(0).getEndDate());
			assertEquals(expectedResult.get(0).getPrice(), result.get(0).getPrice());
		}
	}

	@Test
	public void testPriceAt9PMOnDay14() throws ParseException {
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-06-14 21:00:00");
		Long productId = 35455L;
		Long brandId = 1L;
		List<PriceQuery> expectedResult = new ArrayList<>();
		expectedResult.add(createPriceQuery(35455L, 1L, 3L, date, parseDate("2020-06-15 11:00:00"), 30.50));

		when(this.priceQueryService.queryPrice(eq(date), eq(productId), eq(brandId))).thenReturn(expectedResult);

		List<PriceQuery> result = this.priceQueryController.queryPrice(date, productId, brandId);

		if (result.size() >= 1) {
			assertEquals(expectedResult.size(), result.size());
			assertEquals(expectedResult.get(0).getProductId(), result.get(0).getProductId());
			assertEquals(expectedResult.get(0).getBrandId(), result.get(0).getBrandId());
			assertEquals(expectedResult.get(0).getPriceList(), result.get(0).getPriceList());
			assertEquals(expectedResult.get(0).getStartDate(), result.get(0).getStartDate());
			assertEquals(expectedResult.get(0).getEndDate(), result.get(0).getEndDate());
			assertEquals(expectedResult.get(0).getPrice(), result.get(0).getPrice());
		}
	}

	@Test
	public void testPriceAt10AMOnDay15() throws ParseException {
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-06-15 10:00:00");
		Long productId = 35455L;
		Long brandId = 1L;
		List<PriceQuery> expectedResult = new ArrayList<>();
		expectedResult.add(createPriceQuery(35455L, 1L, 3L, date, parseDate("2020-06-15 11:00:00"), 30.50));

		when(this.priceQueryService.queryPrice(eq(date), eq(productId), eq(brandId))).thenReturn(expectedResult);

		List<PriceQuery> result = this.priceQueryController.queryPrice(date, productId, brandId);

		if (result.size() >= 1) {
			assertEquals(expectedResult.size(), result.size());
			assertEquals(expectedResult.get(0).getProductId(), result.get(0).getProductId());
			assertEquals(expectedResult.get(0).getBrandId(), result.get(0).getBrandId());
			assertEquals(expectedResult.get(0).getPriceList(), result.get(0).getPriceList());
			assertEquals(expectedResult.get(0).getStartDate(), result.get(0).getStartDate());
			assertEquals(expectedResult.get(0).getEndDate(), result.get(0).getEndDate());
			assertEquals(expectedResult.get(0).getPrice(), result.get(0).getPrice());
		}

	}

	@Test
	public void testPriceAt9PMOnDay16() throws ParseException {
		Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-06-16 21:00:00");
		Long productId = 35455L;
		Long brandId = 1L;
		List<PriceQuery> expectedResult = new ArrayList<>();
		expectedResult.add(createPriceQuery(35455L, 1L, 4L, date, parseDate("2020-12-31 23:59:59"), 38.95));

		when(this.priceQueryService.queryPrice(eq(date), eq(productId), eq(brandId))).thenReturn(expectedResult);

		List<PriceQuery> result = this.priceQueryController.queryPrice(date, productId, brandId);

		if (result.size() >= 1) {
			assertEquals(expectedResult.size(), result.size());
			assertEquals(expectedResult.get(0).getProductId(), result.get(0).getProductId());
			assertEquals(expectedResult.get(0).getBrandId(), result.get(0).getBrandId());
			assertEquals(expectedResult.get(0).getPriceList(), result.get(0).getPriceList());
			assertEquals(expectedResult.get(0).getStartDate(), result.get(0).getStartDate());
			assertEquals(expectedResult.get(0).getEndDate(), result.get(0).getEndDate());
			assertEquals(expectedResult.get(0).getPrice(), result.get(0).getPrice());
		}
	}

	private PriceQuery createPriceQuery(Long productId, Long brandId, Long priceList, Date startDate, Date endDate,
			Double price) {
		PriceQuery priceQuery = new PriceQuery();
		priceQuery.setProductId(productId);
		priceQuery.setBrandId(brandId);
		priceQuery.setPriceList(priceList);
		priceQuery.setStartDate(startDate);
		priceQuery.setEndDate(endDate);
		priceQuery.setPrice(price);
		return priceQuery;
	}

	private Date parseDate(String dateStr) throws ParseException {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateStr);
	}
}