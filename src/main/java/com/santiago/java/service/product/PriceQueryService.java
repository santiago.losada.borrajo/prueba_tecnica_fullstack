package com.santiago.java.service.product;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.santiago.java.models.product.PriceQuery;
import com.santiago.java.repository.product.ProductRepository;

@Service
public class PriceQueryService {

	private ProductRepository productRepository;

	@Autowired
	public void ProductRepository(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	public List<PriceQuery> queryPrice(Date date, Long productId, Long brandId) {
		return this.productRepository.findByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandId(date,
				date, productId, brandId);
	}
}
