package com.santiago.java.repository.product;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.santiago.java.models.product.PriceQuery;
import com.santiago.java.models.product.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {

	List<PriceQuery> findByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandId(Date date1, Date date2,
			Long productId, Long brandId);
}
