package com.santiago.java.controller.product;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.santiago.java.models.product.PriceQuery;
import com.santiago.java.service.product.PriceQueryService;

@RestController
public class PriceQueryController {

	private final PriceQueryService priceQueryService;

	public PriceQueryController(PriceQueryService priceQueryService) {
		this.priceQueryService = priceQueryService;
	}

	@GetMapping("/queryPrice")
	public List<PriceQuery> queryPrice(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date date,
			@RequestParam Long productId, @RequestParam Long brandId) {
		return this.priceQueryService.queryPrice(date, productId, brandId);
	}
}
